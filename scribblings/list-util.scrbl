#lang scribble/manual
@require[@for-label[list-util
                    racket/base]]

@title{list-util}
@author[(author+email "Raymond Racine" "ray.racine@gmail.com")]

@section{List Utilities}
@defmodule[list-util]

@defproc[(make-list [len Integer] [elem A]) (Listof A)]{
@racket[(All (a) Integer a -> (Listof a))]

Create a list of length len by repeating the provided elem as necessary.
}

@defproc[(weave [elem A] [lst (Listof A)]) (Listof A)]{
@racket[(All (a) a (Listof a) -> (Listof a))]

Weave an element strictly between two elements of the lst.
}

@defproc[(zip [as (Listof A)] [bs (Listof B)]) (Listof (Pair A B))]{
@racket[(All (A B) (Listof A) (Listof B) -> (Listof (Pair A B)))]

Return a list formed by pairing element by element from the two provided lists.
The length of the returned list will be the same length of the shortest of the provided lists.
}


